const { bufferToHex, keccak256 } = require('ethereumjs-util')
const assert = require('assert')

const [,, vanityInput, lower = 0, upper = 1000000] = process.argv

if (!vanityInput) {
  console.log("Usage:")
  console.log(`  node gen.js 1337`)
  console.log(`  node gen.js startsWith:b33f`)
  console.log(`  node gen.js endsWith:b4115`)
  process.exit()
}

const [toFind, vanityMethod = 'includes'] = vanityInput.split(":").reverse()

assert(['includes','endsWith','startsWith'].includes(vanityMethod))

// 0xff ++ deployingAddress is fixed:
const string1 = '0xffca4dfd86a86c48c5d9c228bedbeb7f218a29c94b'

// Hash of the bytecode is fixed. Calculated with eth.keccak256():
const string2 = '4670da3f633e838c2746ca61c370ba3dbd257b86b28b78449f4185480e2aba51'

const res = run (string1, string2, lower,upper)
if (res) {
  console.log(res)
}
else {
  console.log(`Not found :( ${lower}-${upper}`)
}

function run (string1, string2, lower, upper) {

  // In each loop, i is the value of the salt we are checking
  for (let i = lower; i < upper; i++) {
    // 1. Convert i to hex, and it pad to 32 bytes:
    const saltToBytes = i.toString(16).padStart(64, '0')

    // 2. Concatenate this between the other 2 strings
    const concatString = string1.concat(saltToBytes).concat(string2)

    // 3. Hash the resulting string
    const hashed = bufferToHex(keccak256(concatString))

    // 4. Remove leading 0x and 12 bytes
    // 5. Check if the result contains badc0de
    if (hashed.substr(26)[vanityMethod](toFind)) {
      return saltToBytes
    }
  }
}
