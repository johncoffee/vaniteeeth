const spawn = require('child_process')
const assert = require('assert')
const [,, vanityInput, lower = 0, upper = 1000000, numProcessesIn = 2] = process.argv
const numProcesses = parseInt(numProcessesIn)

if (!vanityInput) {
  console.log(`usage`)
  console.log(`  node run-many.js 123456 250000 500000 4`)
  console.log(`  node run-many.js 00aa 0 5000`)
  console.log(`  node run-many.js startsWith:ba115 0 20000`)
  process.exit()
}

assert(upper % numProcesses === 0, "upper must be divisible by "+numProcesses)
assert(lower % numProcesses === 0, "lower must be divisible by "+numProcesses)

new Array(numProcesses)
  .fill(null)
  .map((_, index) => {
    const piece = (upper-lower)/numProcesses
    const high = piece * (index+1)
    const low = high - piece

    console.log(`starting piece of ${piece} in range ${low},${high} in process ${index+1}/${numProcesses}`)
    const proc = spawn.exec(`node gen.js ${vanityInput} ${low} ${high}`)
    proc.stdout.on('data', data => handleData(index+1, data));
    return proc
  })

function handleData(id, data) {
    console.log(`${id} said:`)
    if (data.startsWith('Not found')) {
      console.log(data);
    }
    else {
      console.log(data)
    }
}
